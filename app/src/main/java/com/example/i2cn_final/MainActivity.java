package com.example.i2cn_final;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Clock;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private RequestQueue reqQueue;
    private static final String TAG = "Main_Activity";
    private static final String TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjdiNDZmNmVkMjhjNzhlMTliOGFhNWExYmNjMGRmMDc1NGI3YjczZjUwMjRkMjljMGU5ZGQzMWQzMzM0YjNkMDliNDQ3NzMyNTQ5ZmVhN2MwIn0.eyJhdWQiOiIyIiwianRpIjoiN2I0NmY2ZWQyOGM3OGUxOWI4YWE1YTFiY2MwZGYwNzU0YjdiNzNmNTAyNGQyOWMwZTlkZDMxZDMzMzRiM2QwOWI0NDc3MzI1NDlmZWE3YzAiLCJpYXQiOjE1NzYxMzEwMjQsIm5iZiI6MTU3NjEzMTAyNCwiZXhwIjoxNjA3NzUzNDI0LCJzdWIiOiIyMTYiLCJzY29wZXMiOltdfQ.Hil_g5KBUvU3JFt23_x0sOv3SCmgXyF_981-plBAQlrI3UZQFkXRf0sPg_DSazF4tpg7D-ZForZmKRDePdc5_1wb_jki8J3Yad6T-zrpC0TALWaXps32esmKZ5USgQZyButJugcUSO1eHf9FEOHqn521indSghN0ghTRBPHW-xvme0JvpZtl2-9blCNlTWJXArFV06q5y4v30UhOJrc24hW5s9vNY2ChyoW7nB7EXvWcn-xJQd-0hIX05QvEg4rREjCGoWdUz09kUcdsNkkfCC9OXjMkhEL_ev4sjIj3NVXLlrtMe5209PI0mK2H-V9wycJeqDqR0cOTYufBZARsK6OfAMaeLwwk14neuLtrt3r5xMVyLHcxX00_vGURuE1CxhBSHijSQZLLG_U7ZCKPUWITvVYwtRaODwhaP54Ro19FdH13OqiF5bLzKcO2IaOPu3vLKPUhIOgazsxKOYyjwZrdovoSLWh9oMZbVxxgmw6pqsjLO1x4ddtUhOkzmOtWEc5Nh1aF4rsZUA0Ph0_JUlucfSv0kU8f1XZXLqWtLEe6xyPKZ5gVJopm-YO_1AFcFxJBXqNjZErKUzumM60yxy5Bv4hsn0d6BHdcknUUcWwZCD8IMhaVRWmbifDaIBC40xOJgZpzgjPtPrDd4cEwVmSdzfEdAzTz-Sn4Rjp9bAU";
    private DateTimeFormatter formatter;  // = ("yyyy-MM-DD HH:mm:ss");
    private TextView statusText, verbText, timeText;
    private ProgressBar progressBar;
    private Handler queryHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        reqQueue = Volley.newRequestQueue(this);
        formatter = DateTimeFormatter.ofPattern("yyyy-MM-DD HH:mm:ss");

        statusText = findViewById(R.id.textView);
        verbText = findViewById(R.id.textView2);
        timeText = findViewById(R.id.textView3);
        progressBar = findViewById(R.id.progressBar4);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showUnknownTheme();
        query();
        queryHandler.postDelayed(timerRun, 3 * 60 * 1000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        queryHandler.removeCallbacks(timerRun);
    }

    private void showUnknownTheme() {
        progressBar.setProgressDrawable(getDrawable(R.drawable.circular_progress_bar));
        progressBar.setProgress(80);
        statusText.setText("狀態");
        verbText.setText("正在");
        timeText.setText("計算中");
    }

    private final Runnable timerRun = new Runnable()
    {
        public void run()
        {
            showUnknownTheme();
            query();
            queryHandler.postDelayed(this, 3 * 60 * 1000);
        }
    };

    public void openStatistic(View view) {
        Intent intent = new Intent(this, Statistics.class);
        startActivity(intent);
    }

    public void openTimer(View view) {
        Intent intent = new Intent(this, TimerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    public void openUserRecord(View view) {
        Intent intent = new Intent(this, UserRecordActivity.class);
        startActivity(intent);
    }

    private float absolute(JSONObject frame) {
        try {
            float accx = Float.parseFloat(frame.getString("acc_x"));
            float accy = Float.parseFloat(frame.getString("acc_y"));
            float accz = Float.parseFloat(frame.getString("acc_z"));
            return Math.abs(accx) + Math.abs(accy) + Math.abs(accz);
        } catch (JSONException | NumberFormatException e) {
            return 0;
        }
    }

    protected String getTargetIntervalString(LocalDateTime start, LocalDateTime end) {
        String startStr = formatter.format(start);
        String endStr = formatter.format(end);

        return startStr + " - " + endStr;
    }

    protected String getCurrentIntervalString() {
        LocalDateTime start, end;

        start = LocalDateTime.now(Clock.system(ZoneId.of("UTC+8")))
                .minus(40, ChronoUnit.MINUTES);
        end = LocalDateTime.now(Clock.system(ZoneId.of("UTC+8")));

        return getTargetIntervalString(start, end);
    }


    public void query() {
        String targetUrl = "https://campus.kits.tw/api/get/data/aa71b242?date_filter=" +
                getCurrentIntervalString();
        Log.d(TAG, targetUrl);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.POST, targetUrl, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d(TAG, "Response: " + response.toString());
                processResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error" + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + TOKEN);
                params.put("Accept", "application/json");
                return params;
            }
        };

        reqQueue.add(jsonArrayRequest);
    }

    public void processResponse(JSONArray res) {
        long secs = -1;
        if (res.length() != 0) {
            try {
                secs = calculateLeftTime(res);
            } catch (JSONException e) {
                e.printStackTrace();
                return;
            }
        }

        Duration left_time;
        if (secs == -1) {
            Log.d(TAG, "Empty");
            statusText.setText("空閒中");
            verbText.setText("目前");
            timeText.setText("可以使用");
            progressBar.setProgressDrawable(getDrawable(R.drawable.green_circular_pb));
            progressBar.setProgress(100);
        } else if (secs == 0) {
            Log.d(TAG, "Maybe");
            statusText.setText("不確定");
            verbText.setText("或許");
            timeText.setText("才剛開始");
            progressBar.setProgressDrawable(getDrawable(R.drawable.circular_progress_bar));
            progressBar.setProgress(0);
        } else {
            left_time = Duration.ofSeconds(secs);
            int minutes = (int) left_time.toMinutes();
            statusText.setText("使用中");
            verbText.setText("還剩");
            timeText.setText(minutes + " 分鐘");
            if (minutes > 15) {
                progressBar.setProgressDrawable(getDrawable(R.drawable.red_circular_pb));
            } else {
                progressBar.setProgressDrawable(getDrawable(R.drawable.circular_progress_bar));
            }
            progressBar.setProgress(minutes * 100 / 35);
        }
    }

    public long calculateLeftTime(JSONArray data) throws JSONException {
        JSONObject lastFrame = data.getJSONObject(data.length() - 1);
        int lastFrameCnt = lastFrame.getInt("frame_cnt");
        LocalDateTime lastTime, nowTime;
        lastTime = LocalDateTime.parse(lastFrame.getString("created_at"), formatter);
        nowTime = LocalDateTime.now(Clock.system(ZoneId.of("UTC+8")));

        Duration offset = Duration.between(lastTime, nowTime), bigOffset = null;
        if (offset.compareTo(Duration.ofMinutes(10)) > 0) {
            return -1;
        }

        int totalCount = 1, error = 0, accuError = 1, bigCount = 0;
        if (absolute(lastFrame) >= 0.12) {
            bigCount++;
            bigOffset = offset;
        }

        for (int i = data.length() - 1; i >= 0; i--) {
            JSONObject frame = data.getJSONObject(i);
            if (frame.getInt("frame_cnt") == lastFrame.getInt("frame_cnt")) {
                continue;
            }

            lastFrame = frame;
            if (frame.get("acc_x") == JSONObject.NULL) {
                error++;
                accuError++;
                continue;
            }
            if (accuError > 0) {
                accuError--;
            }

            LocalDateTime createdTime =
                    LocalDateTime.parse(frame.getString("created_at"), formatter);
            int frameCnt = frame.getInt("frame_cnt");
            int frameDiff = lastFrameCnt - frameCnt;
            Duration timeDiff = Duration.between(createdTime, lastTime);

            if (absolute(lastFrame) >= 0.12) {
                bigCount++;
            }
            if (frameDiff - accuError != 0) {
                if (frameDiff == 1 || frameDiff - accuError == 1) {
                    if (timeDiff.dividedBy(frameDiff - accuError).compareTo(Duration.ofMinutes(8)) > 0) {
                        break;
                    }
                } else if (timeDiff.dividedBy(frameDiff - accuError).compareTo(Duration.ofMinutes(5)) > 0) {
                    break;
                }
            }

            totalCount += frameDiff;
            lastFrameCnt = frameCnt;
            lastTime = createdTime;
            accuError = 0;
        }

        if (bigOffset == null ||
                (bigOffset.compareTo(Duration.ofMinutes(5)) > 0 && bigCount > 10)){
            return -1;
        }

        totalCount -= error;
        int leftCount = 24 - totalCount - totalCount / 8;
        Log.d(TAG, totalCount + "");
        Duration leftTime = Duration.ofSeconds(90).multipliedBy(leftCount).minus(offset);

        if (totalCount <= 0) {
            return -1;
        } else if (totalCount < 3) {
            if (offset.compareTo(Duration.ofMinutes(5)) > 0) {
                return -1;
            } else {
                return 0;
            }
        } else if (leftTime.compareTo(Duration.ZERO) > 0) {
            Log.d(TAG, leftTime.toString());
            if (bigCount == 0) {
                return -1;
            } else if (bigCount == 1) {
                return 0;
            } else {
                return leftTime.getSeconds();
            }
        } else {
            return -1;
        }
    }


}
