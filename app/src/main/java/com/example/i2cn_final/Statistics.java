package com.example.i2cn_final;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Statistics extends AppCompatActivity {

    int data_0=0;
    private int data_4=0;
    private int data_8=0;
    private int data_12=0;
    private int data_16=0;
    private int data_20=0;

    private static final String TAG = "Statistics_Activity";
    private static final String TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjdiNDZmNmVkMjhjNzhlMTliOGFhNWExYmNjMGRmMDc1NGI3YjczZjUwMjRkMjljMGU5ZGQzMWQzMzM0YjNkMDliNDQ3NzMyNTQ5ZmVhN2MwIn0.eyJhdWQiOiIyIiwianRpIjoiN2I0NmY2ZWQyOGM3OGUxOWI4YWE1YTFiY2MwZGYwNzU0YjdiNzNmNTAyNGQyOWMwZTlkZDMxZDMzMzRiM2QwOWI0NDc3MzI1NDlmZWE3YzAiLCJpYXQiOjE1NzYxMzEwMjQsIm5iZiI6MTU3NjEzMTAyNCwiZXhwIjoxNjA3NzUzNDI0LCJzdWIiOiIyMTYiLCJzY29wZXMiOltdfQ.Hil_g5KBUvU3JFt23_x0sOv3SCmgXyF_981-plBAQlrI3UZQFkXRf0sPg_DSazF4tpg7D-ZForZmKRDePdc5_1wb_jki8J3Yad6T-zrpC0TALWaXps32esmKZ5USgQZyButJugcUSO1eHf9FEOHqn521indSghN0ghTRBPHW-xvme0JvpZtl2-9blCNlTWJXArFV06q5y4v30UhOJrc24hW5s9vNY2ChyoW7nB7EXvWcn-xJQd-0hIX05QvEg4rREjCGoWdUz09kUcdsNkkfCC9OXjMkhEL_ev4sjIj3NVXLlrtMe5209PI0mK2H-V9wycJeqDqR0cOTYufBZARsK6OfAMaeLwwk14neuLtrt3r5xMVyLHcxX00_vGURuE1CxhBSHijSQZLLG_U7ZCKPUWITvVYwtRaODwhaP54Ro19FdH13OqiF5bLzKcO2IaOPu3vLKPUhIOgazsxKOYyjwZrdovoSLWh9oMZbVxxgmw6pqsjLO1x4ddtUhOkzmOtWEc5Nh1aF4rsZUA0Ph0_JUlucfSv0kU8f1XZXLqWtLEe6xyPKZ5gVJopm-YO_1AFcFxJBXqNjZErKUzumM60yxy5Bv4hsn0d6BHdcknUUcWwZCD8IMhaVRWmbifDaIBC40xOJgZpzgjPtPrDd4cEwVmSdzfEdAzTz-Sn4Rjp9bAU";
    private RequestQueue reqQueue;
    private DateTimeFormatter formatter;

    private final String[] weekdays = {"mon","00:00", "04:00", "08:00", "12:00", "16:00", "20:00"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        reqQueue = Volley.newRequestQueue(this);
        formatter = DateTimeFormatter.ofPattern("yyyy-MM-DD HH:mm:ss");

        Button button = findViewById(R.id.button);
        button.setOnClickListener(listener);

        query();
    }

    public void query() {
        // 改網址可以取得不同時段的資料
        String targetUrl = "https://campus.kits.tw/api/get/data/aa71b242?date_filter=2020-01-07 - 2020-01-12" ;

        Log.d(TAG, targetUrl);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.POST, targetUrl, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d(TAG, "Response: " + response.toString());
                processResponse(response);
                setBarData();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Handle error
                Log.e(TAG, "Error" + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", "Bearer " + TOKEN);
                params.put("Accept", "application/json");
                return params;
            }
        };

        reqQueue.add(jsonArrayRequest);
    }

    public void processResponse(JSONArray res) {
        // 處理資料
        Log.d(TAG, "Process");
        int time=0;
        int date=7;
        for(int i=0;i<res.length();i++) // = =
        {
            JSONObject frame;
            try {
                frame = res.getJSONObject(i);
                LocalDateTime createdTime = LocalDateTime.parse(frame.getString("created_at"), formatter);
                if(createdTime.getDayOfMonth()>date)
                {
                    date++;
                    time=0;
                }
                if(absolute(frame)>0.12&&createdTime.getHour()*60+createdTime.getMinute()>=time)
                {
                    if(createdTime.getHour()>=0&&createdTime.getHour()<4)data_0++;
                    if(createdTime.getHour()>=4&&createdTime.getHour()<8)data_4++;
                    if(createdTime.getHour()>=8&&createdTime.getHour()<12)data_8++;
                    if(createdTime.getHour()>=12&&createdTime.getHour()<16)data_12++;
                    if(createdTime.getHour()>=16&&createdTime.getHour()<20)data_16++;
                    if(createdTime.getHour()>=20)data_20++;
                    time=createdTime.getHour()*60+createdTime.getMinute()+35;

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        data_0/=5;
        data_4/=5;
        data_8/=5;
        data_12/=5;
        data_16/=5;
        data_20/=5;
    }

    private void setBarData(){
        Log.d(TAG, "Bardata");

        BarChart chart_bar = findViewById(R.id.chart_bar);
        chart_bar.getLegend().setTextSize(16);

        XAxis xAxis = chart_bar.getXAxis();
        xAxis.setDrawLabels(true);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(weekdays));
        xAxis.setTextSize(16);
        xAxis.setTextColor(Color.rgb(32, 32,32));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);

        chart_bar.getDescription().setEnabled(false);
        chart_bar.setPinchZoom(true);
        //xAxis.setLabelCount(6);
        //模擬數據
        ArrayList<BarEntry> yVals1 = new ArrayList<>();
        yVals1.add(new BarEntry(1f, data_0));
        yVals1.add(new BarEntry(2f, data_4));
        yVals1.add(new BarEntry(3f, data_8));
        yVals1.add(new BarEntry(4f, data_12));
        yVals1.add(new BarEntry(5f, data_16));
        yVals1.add(new BarEntry(6f, data_20));

        BarDataSet set1;
        if (chart_bar.getData() != null &&
                chart_bar.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) chart_bar.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            chart_bar.getData().notifyDataChanged();
            chart_bar.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yVals1, "洗衣機使用次數");
            ArrayList<IBarDataSet> dataSets = new ArrayList<>();

            dataSets.add(set1);
            BarData data = new BarData(dataSets);
            data.setValueTextSize(12f);
            data.setBarWidth(0.6f);
            //data.setValueFormatter(new CallCountValueFormatter());
            //設置數據
            chart_bar.setData(data);
        }
        chart_bar.invalidate();
    }

    private float absolute(JSONObject frame) {
        try {
            float accx = Float.parseFloat(frame.getString("acc_x"));
            float accy = Float.parseFloat(frame.getString("acc_y"));
            float accz = Float.parseFloat(frame.getString("acc_z"));
            return Math.abs(accx) + Math.abs(accy) + Math.abs(accz);
        } catch (JSONException | NumberFormatException e) {
            return 0;
        }
    }

    private Button.OnClickListener listener = new Button.OnClickListener(){

        @Override
        public void onClick(View arg0) {
            // TODO Auto-generated method stub
            Intent intent = new Intent();
            intent.setClass(Statistics.this,page2.class);
            startActivity(intent);
            finish();
        }

    };
}

