package com.example.i2cn_final;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.RequiresApi;


@RequiresApi(api = Build.VERSION_CODES.O)
public class TimerActivity extends AppCompatActivity {
    private TextView mTvShow;
    private static CountDownTimer singleTimer;
    private static long leftTime;
    private long[] vibrate = {0, 100, 200, 300};
    private Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        mTvShow = findViewById(R.id.show);
        if (singleTimer != null && leftTime > 0) {
            CountDownTimer temp = createTimer(leftTime);
            temp.start();
            singleTimer.cancel();
            singleTimer = temp;
        }
    }

    public void oncancel(View v) {
        singleTimer.cancel();
    }

    public void restart(View v) {
        singleTimer = createTimer(1000 * 60 * 40);
        singleTimer.start();
    }


    private CountDownTimer createTimer(long totalTime) {
        return new CountDownTimer(totalTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                leftTime = millisUntilFinished;
                mTvShow.setText((millisUntilFinished / 1000) / 60 + "分鐘" + (millisUntilFinished / 1000) % 60 + "秒");
            }

            @Override
            public void onFinish() {
                mTvShow.setEnabled(true);
                mTvShow.setText("時間到！");
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                Intent notifyIntent = new Intent(TimerActivity.this, TimerActivity.class);
                notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PendingIntent appIntent = PendingIntent.getActivity(TimerActivity.this, 0, notifyIntent, 0);

                Notification notification
                        = new Notification.Builder(TimerActivity.this)
                        .setContentIntent(appIntent)
                        .setSmallIcon(R.drawable.ic_launcher_background) // 設置狀態列裡面的圖示（小圖示）　　
                        .setLargeIcon(BitmapFactory.decodeResource(TimerActivity.this.getResources(), R.drawable.ic_launcher_background)) // 下拉下拉清單裡面的圖示（大圖示）
                        .setTicker("notification on status bar.") // 設置狀態列的顯示的資訊
                        .setWhen(System.currentTimeMillis())// 設置時間發生時間
                        .setAutoCancel(false) // 設置通知被使用者點擊後是否清除  //notification.flags = Notification.FLAG_AUTO_CANCEL;
                        .setContentTitle("時間到!") // 設置下拉清單裡的標題
                        .setContentText("洗衣服時間倒數完成!")// 設置上下文內容
                        .setOngoing(true)      //true使notification變為ongoing，用戶不能手動清除// notification.flags = Notification.FLAG_ONGOING_EVENT; notification.flags = Notification.FLAG_NO_CLEAR;
                        .setDefaults(Notification.DEFAULT_ALL) //使用所有預設值，比如聲音，震動，閃屏等等

                        .setVibrate(vibrate) //自訂震動長度

                        .build();

                // 將此通知放到通知欄的"Ongoing"即"正在運行"組中
                notification.flags = Notification.FLAG_ONGOING_EVENT;
                notification.flags = Notification.FLAG_NO_CLEAR;

                //閃爍燈光
                notification.flags = Notification.FLAG_SHOW_LIGHTS;

                // 重複的聲響,直到用戶響應。
                notification.flags = Notification.FLAG_INSISTENT;

                mNotificationManager.notify(0, notification);
            }
        };
    }


}